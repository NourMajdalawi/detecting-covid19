# Detecting COVID19
This project-based on Python code. The models have been built using the PyTorch library and Resnet18 model.
Feel free to test it and play around with the code by click on the Binder icon below.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/NourMajdalawi%2Fdetecting-covid19/master/?filepath=Detecting_COVID19_Final_version.ipynb)

